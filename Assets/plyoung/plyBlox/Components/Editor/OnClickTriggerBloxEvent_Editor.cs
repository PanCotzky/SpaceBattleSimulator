﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEditor;
using System.Collections;
using plyBloxKit;

// OnClickTriggerBloxEvent_v3
// by RaiuLyn
// https://twitter.com/RaiuLyn
// http://raiulyn.wordpress.com 
// http://forum.plyoung.com/users/raiulyn
// ============================================================================================================
namespace Lyn
{
    [CustomEditor(typeof(OnClickTriggerBloxEvent))]
    public class OnClickTriggerBloxEvent_Editor : Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            OnClickTriggerBloxEvent evt = (OnClickTriggerBloxEvent)target;
            evt.eventName = EditorGUILayout.TextField("Event Name", evt.eventName);
            
            EditorGUILayout.PropertyField(serializedObject.FindProperty("blox"));
            if (GUILayout.Button("Add Parameter"))
            {
                evt.Params.Add(new EventParam("new", ParamScope.Temp, null));
            }
            for (int i = 0; i < serializedObject.FindProperty("Params").arraySize; i++)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("Params").GetArrayElementAtIndex(i));
                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Move up"))
                {
                    serializedObject.FindProperty("Params").MoveArrayElement(i, i-1);
                }
                if (GUILayout.Button("Move down"))
                {
                    serializedObject.FindProperty("Params").MoveArrayElement(i, i+1);
                }
                EditorGUILayout.EndHorizontal();
                if (GUILayout.Button("Delete"))
                {
                    serializedObject.FindProperty("Params").DeleteArrayElementAtIndex(i);
                }
            }
            serializedObject.ApplyModifiedProperties();
        }
    }

    [CustomPropertyDrawer(typeof(EventParam))]
    public class ParamDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty ClickOp = property.FindPropertyRelative("handlerType");
            SerializedProperty Input = property.FindPropertyRelative("mouseInput");
            SerializedProperty Keycode = property.FindPropertyRelative("keyCode");
            SerializedProperty DebugM = property.FindPropertyRelative("DebugMsg");

            SerializedProperty Param = property.FindPropertyRelative("ParamName");
            SerializedProperty Scope = property.FindPropertyRelative("_scope");
            SerializedProperty Type = property.FindPropertyRelative("_type");
            SerializedProperty String = property.FindPropertyRelative("str_value");
            SerializedProperty Int = property.FindPropertyRelative("int_value");
            SerializedProperty Float = property.FindPropertyRelative("float_value");
            SerializedProperty Bool = property.FindPropertyRelative("bool_value");
            SerializedProperty Vector3 = property.FindPropertyRelative("vector3_value");
            SerializedProperty Vector2 = property.FindPropertyRelative("vector2_value");
            SerializedProperty Gameobject = property.FindPropertyRelative("gameobject_value");
            SerializedProperty Component = property.FindPropertyRelative("component_value");
            SerializedProperty Object = property.FindPropertyRelative("object_value");
            SerializedProperty Item = property.FindPropertyRelative("item_value");
            SerializedProperty ItemSearch = property.FindPropertyRelative("item_search");

            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox, GUILayout.Height(220), GUILayout.ExpandWidth(true));
            Rect SettingsRect = EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.ExpandHeight(true));
            Rect ClickOpPrefixRect = new Rect(SettingsRect.x, SettingsRect.y + 10, SettingsRect.width, 15);
            EditorGUI.PrefixLabel(ClickOpPrefixRect, new GUIContent("Click Option", "On Click options"));
            Rect ClickOpRect = new Rect(SettingsRect.x, SettingsRect.y + 30, SettingsRect.width - 10, 15); 
            EditorGUI.PropertyField(ClickOpRect, ClickOp, GUIContent.none);
            
            Rect InputPrefixRect = new Rect(SettingsRect.x, SettingsRect.y + 60, SettingsRect.width, 15);
            EditorGUI.PrefixLabel(InputPrefixRect, new GUIContent("Mouse Input", "Method of the Click"));
            Rect InputRect = new Rect(SettingsRect.x, SettingsRect.y + 80, SettingsRect.width - 10, 15);
            EditorGUI.PropertyField(InputRect, Input, GUIContent.none);

            Rect KeycodePrefixRect = new Rect(SettingsRect.x, SettingsRect.y + 100, SettingsRect.width, 15);
            EditorGUI.PrefixLabel(KeycodePrefixRect, new GUIContent("+", ""));
            Rect KeycodeRect = new Rect(SettingsRect.x + 15, SettingsRect.y + 100, SettingsRect.width - 20, 15);
            EditorGUI.PropertyField(KeycodeRect, Keycode, GUIContent.none);

            Rect DebugPrefixRect = new Rect(SettingsRect.x, SettingsRect.y + 120, SettingsRect.width, 15);
            EditorGUI.PrefixLabel(DebugPrefixRect, new GUIContent("Debug", "Shows Debug Lines in Debug.Log"));
            Rect DebugRect = new Rect(SettingsRect.x + 3, SettingsRect.y + 138, SettingsRect.width, 15);
            EditorGUI.PropertyField(DebugRect, DebugM, GUIContent.none);
            EditorGUILayout.EndVertical();
            //
            Rect ParamValueRect = EditorGUILayout.BeginVertical();
            Rect ScopePrefixRect = new Rect(ParamValueRect.x, ParamValueRect.y + 10, ParamValueRect.width, 15);
            EditorGUI.PrefixLabel(ScopePrefixRect, new GUIContent("Scope", "Scope of variable"));
            Rect ScopeRect = new Rect(ParamValueRect.x, ParamValueRect.y + 30, ParamValueRect.width, 15);
            EditorGUI.PropertyField(ScopeRect, Scope, GUIContent.none);

            Rect ParamPrefixRect = new Rect(ParamValueRect.x, ParamValueRect.y + 60, ParamValueRect.width, 15);
            EditorGUI.PrefixLabel(ParamPrefixRect, new GUIContent("Param Name", "Name of the Temp variable"));
            Rect ParamRect = new Rect(ParamValueRect.x, ParamValueRect.y + 80, ParamValueRect.width, 15);
            EditorGUI.PropertyField(ParamRect, Param, GUIContent.none);

            Rect TypePrefixRect = new Rect(ParamValueRect.x, ParamValueRect.y + 110, ParamValueRect.width, 15);
            EditorGUI.PrefixLabel(TypePrefixRect, new GUIContent("Type", "Type of the Temp variable"));
            Rect TypeRect = new Rect(ParamValueRect.x, ParamValueRect.y + 130, ParamValueRect.width, 15);
            EditorGUI.PropertyField(TypeRect, Type, GUIContent.none);

            Rect ValuePrefixRect = new Rect(ParamValueRect.x, ParamValueRect.y + 160, ParamValueRect.width, 15);
            Rect ValueRect = new Rect(ParamValueRect.x, ParamValueRect.y + 180, ParamValueRect.width, 15);
            switch (Type.enumValueIndex)
            {
                case 0:
                    break;
                case 1:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, String, GUIContent.none);
                    break;
                case 2:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Int, GUIContent.none);
                    break;
                case 3:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Float, GUIContent.none);
                    break;
                case 4:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Bool, GUIContent.none);
                    break;
                case 5:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Vector3, GUIContent.none);
                    break;
                case 6:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Vector2, GUIContent.none);
                    break;
                case 7:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Gameobject, GUIContent.none);
                    break;
                case 8:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Component, GUIContent.none);
                    break;
                case 9:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Object, GUIContent.none);
                    break;
                case 10:
                    EditorGUI.PrefixLabel(ValuePrefixRect, new GUIContent("Value", "Value of the Temp variable"));
                    EditorGUI.PropertyField(ValueRect, Item, GUIContent.none);
                    Rect ItemSearchPrefixRect = new Rect(SettingsRect.x, SettingsRect.y + 160, SettingsRect.width, 15);
                    EditorGUI.PrefixLabel(ItemSearchPrefixRect, new GUIContent("Ident Type", "Type to search item"));
                    Rect ItemSearchRect = new Rect(SettingsRect.x, SettingsRect.y + 180, SettingsRect.width, 15);
                    switch (ItemSearch.enumValueIndex)
                    {
                        case 0:
                            EditorGUI.PropertyField(ItemSearchRect, ItemSearch, GUIContent.none);
                            break;
                        case 1:
                            EditorGUI.PropertyField(ItemSearchRect, ItemSearch, GUIContent.none);
                            break;
                        case 2:
                            EditorGUI.PropertyField(ItemSearchRect, ItemSearch, GUIContent.none);
                            break;
                        case 3:
                            EditorGUI.PropertyField(ItemSearchRect, ItemSearch, GUIContent.none);
                            break;
                    }
                    break;
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();

            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty Type = property.FindPropertyRelative("_type");

            switch (Type.enumValueIndex)
            {
                default:
                    return base.GetPropertyHeight(property, label);
                case 0:
                    return base.GetPropertyHeight(property, label);
                case 1:
                    return base.GetPropertyHeight(property, label);
                case 2:
                    return base.GetPropertyHeight(property, label);
                case 3:
                    return base.GetPropertyHeight(property, label);
                case 4:
                    return base.GetPropertyHeight(property, label);
                case 5:
                    return base.GetPropertyHeight(property, label);
                case 6:
                    return base.GetPropertyHeight(property, label);
                case 7:
                    return base.GetPropertyHeight(property, label);
                case 8:
                    return base.GetPropertyHeight(property, label);
                case 9:
                    return base.GetPropertyHeight(property, label);
            }
        }
    }

}

