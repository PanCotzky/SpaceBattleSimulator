﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof (MF_AbstractClassify.FactionsBlock))]
public class FactionsBlock : PropertyDrawer {

	int lh1 = 18; // line height

	public override float GetPropertyHeight( SerializedProperty prop, GUIContent label ) {
		SerializedProperty enemiesProp = prop.FindPropertyRelative("enemies");
		SerializedProperty alliesProp = prop.FindPropertyRelative("allies");
		SerializedProperty neutralProp = prop.FindPropertyRelative("neutral");

		float height = lh1;
		height += lh1 + ( lh1 * Mathf.Max( enemiesProp.arraySize - 1, 0f ) ) + 8f;
		height += lh1 + ( lh1 * Mathf.Max( alliesProp.arraySize - 1, 0f ) ) + 8f;
		height += lh1 + ( lh1 * Mathf.Max( neutralProp.arraySize - 1, 0f ) );

		return height;
	}
	
	// Draw the property inside the given rect
	public override void OnGUI ( Rect position, SerializedProperty prop, GUIContent label ) {
		EditorGUI.BeginProperty (position, label, prop);

		int indent = EditorGUI.indentLevel;

		SerializedProperty enemiesProp = prop.FindPropertyRelative("enemies");
		SerializedProperty alliesProp = prop.FindPropertyRelative("allies");
		SerializedProperty neutralProp = prop.FindPropertyRelative("neutral");

		float height = position.y;

		Rect titleRect = 			new Rect ( position.x, 			height, 			position.width, 		lh1 );
		EditorGUI.LabelField( titleRect, "Factions:", EditorStyles.boldLabel );
		height += lh1;

		// enemies
		Rect enemiesRect = 			new Rect ( position.x, 			height, 			position.width, 		lh1 );
		Rect ctrlRemE = 			new Rect ( position.x+70f, 		height+1f, 			20f, 					lh1-6f );
		Rect ctrlAddE = 			new Rect ( position.x+93f, 		height+1f, 			20f, 					lh1-6f );
		EditorGUI.LabelField( enemiesRect, "Enemies", EditorStyles.label );
		if ( GUI.Button( ctrlRemE, "-" ) ) {
			enemiesProp.DeleteArrayElementAtIndex( enemiesProp.arraySize-1 );
		}
		if ( GUI.Button( ctrlAddE, "+" ) ) {
			enemiesProp.InsertArrayElementAtIndex( enemiesProp.arraySize );
		}
		for ( int i=0; i < enemiesProp.arraySize; i++ ) {
			// draw every element of the array
			Rect valueRect = 		new Rect ( position.x+120f, 		height + ( lh1 * i ), 		position.width-120f,			lh1 );
			EditorGUI.PropertyField( valueRect, enemiesProp.GetArrayElementAtIndex(i), GUIContent.none );
		}
		height += lh1 + ( lh1 * Mathf.Max( enemiesProp.arraySize - 1, 0f ) ) + 8f;

		// allies
		Rect alliesRect = 			new Rect ( position.x, 			height, 			position.width, 		lh1 );
		Rect ctrlRemA = 			new Rect ( position.x+70f, 		height+1f, 			20f, 					lh1-6f );
		Rect ctrlAddA = 			new Rect ( position.x+93f, 		height+1f, 			20f, 					lh1-6f );
		EditorGUI.LabelField( alliesRect, "Allies", EditorStyles.label );
		if ( GUI.Button( ctrlRemA, "-" ) ) {
			alliesProp.DeleteArrayElementAtIndex( alliesProp.arraySize-1 );
		}
		if ( GUI.Button( ctrlAddA, "+" ) ) {
			alliesProp.InsertArrayElementAtIndex( alliesProp.arraySize );
		}
		for ( int i=0; i < alliesProp.arraySize; i++ ) {
			// draw every element of the array
			Rect valueRect = 		new Rect ( position.x+120f, 		height + ( lh1 * i ), 		position.width-120f,			lh1 );
			EditorGUI.PropertyField( valueRect, alliesProp.GetArrayElementAtIndex(i), GUIContent.none );
		}
		height += lh1 + ( lh1 * Mathf.Max( alliesProp.arraySize - 1, 0f ) ) + 8f;

		// neutral
		Rect neutralRect = 			new Rect ( position.x, 			height, 			position.width, 		lh1 );
		Rect ctrlRemN = 			new Rect ( position.x+70f, 		height+1f, 			20f, 					lh1-6f );
		Rect ctrlAddN = 			new Rect ( position.x+93f, 		height+1f, 			20f, 					lh1-6f );
		EditorGUI.LabelField( neutralRect, "Neutral", EditorStyles.label );
		if ( GUI.Button( ctrlRemN, "-" ) ) {
			neutralProp.DeleteArrayElementAtIndex( neutralProp.arraySize-1 );
		}
		if ( GUI.Button( ctrlAddN, "+" ) ) {
			neutralProp.InsertArrayElementAtIndex( neutralProp.arraySize );
		}
		for ( int i=0; i < neutralProp.arraySize; i++ ) {
			// draw every element of the array
			Rect valueRect = 		new Rect ( position.x+120f, 		height + ( lh1 * i ), 		position.width-120f,			lh1 );
			EditorGUI.PropertyField( valueRect, neutralProp.GetArrayElementAtIndex(i), GUIContent.none );
		}

		EditorGUI.indentLevel = indent;
		EditorGUI.EndProperty ();
	}
}


