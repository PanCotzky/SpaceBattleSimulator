﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof (MF_B_Spawner.SpawnType))]
public class SpawnType : PropertyDrawer {

	int lh1 = 18; // line height

	public override float GetPropertyHeight( SerializedProperty property, GUIContent label ) {
		return ( lh1 * 3 ) + 4f;
	}

	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
		EditorGUI.BeginProperty (position, label, property);
		
		// Don't make child fields be indented
		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		float top = position.y;
		float left = 0f;
		float width = 0f;

		top += 2f;
		left = 20f;
		width = 40f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent( "Chance", "Spawn chance. Based on the sum of all spawn chances."), EditorStyles.miniLabel );

		left += width + 10f;
		width = 165f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent( "Prefab", "Prefab to be spawned."), EditorStyles.miniLabel );

		top += lh1-2f;
		left = 20f;
		width = 40f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), property.FindPropertyRelative ("chance"), GUIContent.none );

		left += width + 10f;
		width = 250f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), property.FindPropertyRelative ("prefab"), GUIContent.none );

		top += lh1;
		left = 70f;
		width = 25f;
		EditorGUI.LabelField(  new Rect ( left, top, width, lh1 ), new GUIContent( "Pool", "Use object pooling for this spawn."), EditorStyles.miniLabel ); 
		left += width;
		width = 30f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1 ), property.FindPropertyRelative ("objectPool"), GUIContent.none );

		left += width - 5f;
		width = 48f;
		EditorGUI.LabelField(  new Rect ( left, top, width, lh1 ), new GUIContent( "Add Pool", "Each entry will add this many object to the pool once the minimum pool size is reached."), EditorStyles.miniLabel ); 
		left += width;
		width = 40f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), property.FindPropertyRelative ("addPool"), GUIContent.none );

		left += width + 10f;
		width = 47f;
		EditorGUI.LabelField(  new Rect ( left, top, width, lh1 ), new GUIContent( "Min Pool", "The minimum pool size for this object."), EditorStyles.miniLabel ); 
		left += width;
		width = 40f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), property.FindPropertyRelative ("minPool"), GUIContent.none );
		
		// Set indent back to what it was
		EditorGUI.indentLevel = indent;
		
		EditorGUI.EndProperty ();
	}
}

