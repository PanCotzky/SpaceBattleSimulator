﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof (MF_B_Scanner.MatchBlock))]
public class MatchBlock : PropertyDrawer {

	int lh1 = 18; // line height

	public override float GetPropertyHeight( SerializedProperty prop, GUIContent label ) {
		float lines = 0;
		SerializedProperty matchProp = prop.FindPropertyRelative("matchBy");
		
		if ( matchProp.enumValueIndex == (int)MFnum.MatchType.Faction ) {
			lines = Mathf.Max( prop.FindPropertyRelative("targetableFactions").arraySize - 1, 0f );
		}

		return (lh1 * 3f) + ( lh1 * lines );
	}

	public override void OnGUI ( Rect position, SerializedProperty prop, GUIContent label ) {
		EditorGUI.BeginProperty (position, label, prop);

		int indent = EditorGUI.indentLevel;

		SerializedProperty methodProp = prop.FindPropertyRelative("scanMethod");
		SerializedProperty matchProp = prop.FindPropertyRelative("matchBy");
		SerializedProperty relationProp = prop.FindPropertyRelative("targetableRelation");
		SerializedProperty factionsProp = prop.FindPropertyRelative("targetableFactions");

		Rect buttonRect = 			new Rect (position.x + 150f, 	position.y-lh1-2f, 			120f, 					lh1 );
		Rect methodRect = 			new Rect (position.x, 			position.y, 				position.width, 		lh1 );
		Rect matchRect = 			new Rect (position.x, 			position.y+(lh1), 			position.width, 		lh1 );
		Rect arrayRect = 			new Rect (position.x, 			position.y+(lh1*2f), 		position.width, 		lh1 );

		Rect ctrlRem = 				new Rect ( position.x+120f, 	position.y+(lh1*2f)+1f, 		20f, 					lh1-6f );
		Rect ctrlAdd = 				new Rect ( position.x+143f, 	position.y+(lh1*2f)+1f, 		20f, 					lh1-6f );

		if ( methodProp.enumValueIndex == (int)MFnum.ScanMethodType.Layers ) {
			if ( GUI.Button( buttonRect, "Rebuild Layermask") ) {
				( prop.serializedObject.targetObject as MF_B_Scanner ).BuildLayermask();
			}
		}
		EditorGUI.PropertyField( methodRect, methodProp, new GUIContent("Scan Method") );
		EditorGUI.PropertyField( matchRect, matchProp, new GUIContent("Match By") );

		if ( matchProp.enumValueIndex == (int)MFnum.MatchType.Relation ) {
			EditorGUI.PropertyField( arrayRect, relationProp, new GUIContent("Targetable Relation") );

		} else if ( matchProp.enumValueIndex == (int)MFnum.MatchType.Faction ) {
			EditorGUI.LabelField( arrayRect, "Targetable Factions", EditorStyles.label );
			if ( GUI.Button( ctrlRem, "-" ) ) {
				factionsProp.DeleteArrayElementAtIndex( factionsProp.arraySize-1 );
			}
			if ( GUI.Button( ctrlAdd, "+" ) ) {
				factionsProp.InsertArrayElementAtIndex( factionsProp.arraySize );
			}
			for ( int i=0; i < factionsProp.arraySize; i++ ) {
				// draw every element of the array
				Rect valueRect = 		new Rect ( position.x+170f, 		position.y + (lh1*2f) + ( lh1 * i ), 		position.width-170f,			lh1 );
				EditorGUI.PropertyField( valueRect, factionsProp.GetArrayElementAtIndex(i), GUIContent.none );
			}
		}


		EditorGUI.indentLevel = indent;
		EditorGUI.EndProperty ();
	}
}


