﻿using UnityEngine;
using UnityEditor;
//using System.Collections;
using System.Reflection;

[CustomPropertyDrawer (typeof (InfoAttribute))]
public class InfoDrawer : PropertyDrawer {

	float lh1 = 14f; // line height

	public override float GetPropertyHeight ( SerializedProperty property, GUIContent label ) {
		InfoAttribute Attribute = (InfoAttribute)attribute;
		return ( Attribute.lines * lh1 );
	}
	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label ) {
		InfoAttribute Attribute = (InfoAttribute)attribute;

		Rect textR = new Rect ( position.x, 	position.y, 		position.width,			Attribute.lines * lh1 );
		string text = "Error: No string found.\nInfo attribute shpuld be used with a string field.";
		if ( property.type == "string" ) {
			text = property.stringValue;
		}
		EditorGUI.HelpBox( textR, text, MessageType.None );
	}
}

[CustomPropertyDrawer (typeof (Split1Attribute))]
public class Split1Drawer : PropertyDrawer {

	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label ) {
		Split1Attribute Attribute = (Split1Attribute)attribute;
		float percent = Attribute.splitPercent * .01f;

		float padding = 5f; // space on either side of split line
		float split = ( position.width * percent ) - padding;
		float modLength = 0f;
		if ( Attribute.shortField == true ) {
			modLength = split * .25f;
		}
		float length = split * .5f;
		float height = GetPropertyHeight( property, GUIContent.none );

		Rect labelRect = new Rect ( position.x, 									position.y, 		length + modLength,			height );
		Rect valueRect = new Rect ( position.x + split + modLength - length, 		position.y, 		length - modLength,			height );

		EditorGUI.LabelField( labelRect, new GUIContent( label.text, Attribute.tooltip ), EditorStyles.label );
		EditorGUI.PropertyField( valueRect, property, GUIContent.none );
	}
}



[CustomPropertyDrawer (typeof (Split2Attribute))]
public class Split2Drawer : PropertyDrawer {

	public override float GetPropertyHeight ( SerializedProperty property, GUIContent label ) {
		return -2f; // remove vertical padding and don't create a new line
	}

	public override void OnGUI ( Rect position, SerializedProperty property, GUIContent label ) {
		Split2Attribute Attribute = (Split2Attribute)attribute;
		float percent = Attribute.splitPercent * .01f;
		float padding = 5f; // space on either side of split line
		float split = ( position.width * percent ) - padding;
		float invertSplit = ( position.width * (1f - percent) ) - padding;
		float invertLength = invertSplit * .5f;
		float start = position.x + split + ( padding * 2f );
		float modLength = 0f;
		if ( Attribute.shortField == true ) {
			modLength = invertLength * .5f;
		}

		Rect labelRect = new Rect ( start, 												position.y - Attribute.height, 		invertLength + modLength,			Attribute.height-2f );
		Rect valueRect = new Rect ( start + invertLength + modLength,				 	position.y - Attribute.height, 		invertLength - modLength,			Attribute.height-2f );

		EditorGUI.LabelField( labelRect, new GUIContent( label.text, Attribute.tooltip ), EditorStyles.label );
		EditorGUI.PropertyField( valueRect, property, GUIContent.none );
	}
}


