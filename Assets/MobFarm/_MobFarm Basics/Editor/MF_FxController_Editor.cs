﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof (MF_FxController))]
public class MF_FxController_Editor : Editor {

	SerializedObject obj;
	public MF_FxController script;

	SerializedProperty detachAt;
	SerializedProperty transferMotion;
	SerializedProperty fxList;

	//	void OnEnable () {
	//		obj = new SerializedObject( target );
	//	}

	public override void OnInspectorGUI ( ) {
		obj = new SerializedObject( target );
		obj.Update();
		script = (MF_FxController) target;
		detachAt = obj.FindProperty("detachAt");
		transferMotion = obj.FindProperty("transferMotion");
		fxList = obj.FindProperty("fxList");

		EditorGUILayout.LabelField( new GUIContent("Detach Options:", "When this unit becomes disabled or destroyed, Fx_Controller will detach from object to allow fx to finish."), EditorStyles.boldLabel );
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField( new GUIContent("Detach At", "When to detach the MF_FxController object from the unit."), GUILayout.Width(60f) );
		EditorGUILayout.PropertyField( detachAt, GUIContent.none, GUILayout.Width(85f) );
		GUILayout.Space(10f);
		EditorGUILayout.LabelField( new GUIContent("Transfer Motion", "When detaching, transfer unit rigidbody properties to the rigidbody on this object."), GUILayout.Width(95f) );
		EditorGUILayout.PropertyField( transferMotion, GUIContent.none );
		EditorGUILayout.EndHorizontal();

		GUILayout.Space(8f);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField( new GUIContent("Fx List"), EditorStyles.boldLabel, GUILayout.Width(50f) );
//		EditorGUILayout.PropertyField(fxList, GUIContent.none, true, GUILayout.Width(160f) );


		if ( GUILayout.Button( new GUIContent("-", "Remove"), EditorStyles.miniButton, GUILayout.Width(30f), GUILayout.Height(13f) ) ) {
			fxList.DeleteArrayElementAtIndex( fxList.arraySize-1 );
		}
		if ( GUILayout.Button( new GUIContent("+", "Add"), EditorStyles.miniButton, GUILayout.Width(30f), GUILayout.Height(13f) ) ) {
			fxList.InsertArrayElementAtIndex( fxList.arraySize );
		}
		EditorGUILayout.EndHorizontal();
		for ( int i=0; i < fxList.arraySize; i++ ) {
			// draw every element of the array
			EditorGUILayout.PropertyField( fxList.GetArrayElementAtIndex(i), GUIContent.none );
		}

		obj.ApplyModifiedProperties();
	}
}


[CustomPropertyDrawer (typeof (MF_FxController.FxItem))]
public class FxItem : PropertyDrawer {

	int lh1 = 18; // line height

	public override float GetPropertyHeight( SerializedProperty prop, GUIContent label ) {
		return ( lh1 * 4 ) + 4f;
	}

	// Draw the property inside the given rect
	public override void OnGUI ( Rect position, SerializedProperty prop, GUIContent label ) {
		EditorGUI.BeginProperty (position, label, prop);

		int editorIndent = EditorGUI.indentLevel;

		SerializedProperty obj = prop.FindPropertyRelative("obj"); 
		SerializedProperty active = prop.FindPropertyRelative("active");
		SerializedProperty op = prop.FindPropertyRelative("op");
		SerializedProperty percent = prop.FindPropertyRelative("percent");
		SerializedProperty location = prop.FindPropertyRelative("location");
		SerializedProperty alive = prop.FindPropertyRelative("alive");
		SerializedProperty once = prop.FindPropertyRelative("once");
		SerializedProperty duration = prop.FindPropertyRelative("duration");
		SerializedProperty minTime = prop.FindPropertyRelative("minTime");
		SerializedProperty maxTime = prop.FindPropertyRelative("maxTime");
		SerializedProperty sendValue = prop.FindPropertyRelative("sendValue");
		SerializedProperty invertValue = prop.FindPropertyRelative("invertValue");

		EditorGUI.indentLevel = 1;

		float top = position.y;
		float left = 0f;
		float width = 0f;

		GUI.enabled = false;
		GUIStyle styleHR = new GUIStyle(GUI.skin.box);
		styleHR.stretchWidth = true;
		styleHR.fixedHeight = 2f;

		top += 2f;
		GUI.Box(  new Rect ( position.x, top, 300f, 5f ), "", styleHR);
		GUI.enabled = true;

		top += 2f;
		left = 0f;
		width = 165f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), "Fx Object", EditorStyles.miniLabel );

		left += width;
		width = 80f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), "Location", EditorStyles.miniLabel );


		top += lh1-2f;
		left = 0f;
		width = 175f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), obj, GUIContent.none );

		left += width - 10f;
		width = 160f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), location, GUIContent.none );

		top += lh1;
		left = 0f;
		width = 75f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("", "Fx object will be active when this condition is true. For stat comparisons, use percentage values."), EditorStyles.label );
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1 ), active, GUIContent.none );

		if ( active.enumValueIndex != (int)MF_FxController.FxState.Alive && active.enumValueIndex != (int)MF_FxController.FxState.Dying && active.enumValueIndex != (int)MF_FxController.FxState.Dead ) { 
			left += width - 10f;
			width = 85f;
			EditorGUI.PropertyField( new Rect ( left, top, width, lh1 ), op, GUIContent.none );

			if ( op.enumValueIndex != (int)MF_FxController.FxOp.Increases && op.enumValueIndex != (int)MF_FxController.FxOp.Decreases && op.enumValueIndex != (int)MF_FxController.FxOp.Changes ) { 
				left += width - 10f;
				width = 50f;
				EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), percent, GUIContent.none );
			}

			left += width;
			width = 50f;
			EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("Alive", "This fx will only play if unit is also enabled."), EditorStyles.label );

			left += width - 15f;
			width = 35f;
			EditorGUI.PropertyField( new Rect ( left, top, width, lh1 ), alive, GUIContent.none );
			left += -20f;

		}

		left += width + 10f;
		width = 50f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("Once", "This fx will only play once."), EditorStyles.label );

		left += width - 15f;
		width = 35f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1 ), once, GUIContent.none );

		if ( active.enumValueIndex != (int)MF_FxController.FxState.Shield && active.enumValueIndex != (int)MF_FxController.FxState.Armor && active.enumValueIndex != (int)MF_FxController.FxState.Health &&
			active.enumValueIndex != (int)MF_FxController.FxState.Energy && active.enumValueIndex != (int)MF_FxController.FxState.Monitor ) {
			GUI.enabled = false;
		}
		top += lh1;
		left = 0f;
		width = 48f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("Send", "Send the compared value to fx object script, typically to control intensity.\n\n" +
			"Particle System: Sets rateOverTimeMultipler and rateOverDistanceMultipler.\n\n" +
			"Trail Renderer: Sets time as a percent of the original time.\n\n" +
			"MF_LensFlare: Sets multiplier.\n\n" +
			"Other Objects: No effect."), EditorStyles.label );

		left += width - 15f;
		width = 30f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), sendValue, GUIContent.none );

		if ( sendValue.boolValue == false ) { GUI.enabled = false; }
		left += width - 10f;
		width = 52f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("Invert", "Invert the value sent to the fx object script."), EditorStyles.label );

		left += width - 15f;
		width = 30f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), invertValue, GUIContent.none );
		GUI.enabled = true;

		left += width;
		width = 70f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("Duration", "This fx has specific duration values."), EditorStyles.label );

		left += width - 15f;
		width = 30f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1 ), duration, GUIContent.none );

		GUI.enabled = duration.boolValue;
		left += width -10f;
		width = 40f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("Min", "This fx will be active for a minimum amount of time, even if its active condition becomes false."), EditorStyles.label );

		left += width - 15f;
		width = 40f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), minTime, GUIContent.none );

		left += width - 5f;
		width = 42f;
		EditorGUI.LabelField( new Rect ( left, top, width, lh1 ), new GUIContent("Max", "This fx will be active for a maximum amount of time, even if its active condition is still true."), EditorStyles.label );

		left += width - 15f;
		width = 40f;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), maxTime, GUIContent.none );
		GUI.enabled = true;


		EditorGUI.indentLevel = editorIndent;
		EditorGUI.EndProperty ();
	}
}


