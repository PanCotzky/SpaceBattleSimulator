﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof (MF_AbstractPlatformControl.WeaponData))]
public class WeaponData : PropertyDrawer {

	int lh1 = 18; // line height

	public override float GetPropertyHeight( SerializedProperty property, GUIContent label ) {
		return lh1 + 4f;
	}

	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
		EditorGUI.BeginProperty (position, label, property);
		
		// Don't make child fields be indented
		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		float top = position.y;
		float left = 0f;
		float width = 0f;

		left = 50f;
		width = position.width - left;
		EditorGUI.PropertyField( new Rect ( left, top, width, lh1-2f ), property.FindPropertyRelative ("weapon"), GUIContent.none );
		
		// Set indent back to what it was
		EditorGUI.indentLevel = indent;
		
		EditorGUI.EndProperty ();
	}
}

