﻿using System;
using System.Collections;
using System.Collections.Generic;
using plyCommon;
using UnityEngine;

public class TargetTags : MonoBehaviour
{
    public TargetTypes[] TagsList = new TargetTypes[0];
    
    [HideInInspector]
    public TargetTypes Tags;
    
    private void Awake()
    {
        foreach (TargetTypes type in TagsList)
        {
            Tags = Tags | type;
        }
    }
}

[Flags]
public enum TargetTypes
{
    Ship = 1,
    Missile = 1 << 2,
}
