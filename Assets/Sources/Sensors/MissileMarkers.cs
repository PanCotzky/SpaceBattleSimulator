﻿using System.Collections;
using System.Collections.Generic;
using Chronos.Reflection.Internal;
using PathologicalGames;
using UnityEngine;

public class MissileMarkers : MonoBehaviour
{
    private Dictionary<Transform, Transform> _markers;
    
    public MF_B_TargetList TargetList;
    public Transform TargetMarkerPrefab;
    
    private HudManager _hud;

    void Start()
    {
        _markers = new Dictionary<Transform, Transform>();
        _hud = FindObjectOfType<HudManager>();
    }

    // Update is called once per frame
    void Update()
    {
        var activeTargets = new HashSet<Transform>();
        var newTargets = new Dictionary<Transform, Transform>();
        
        
        foreach (KeyValuePair<int,MF_B_TargetList.TargetData> data in TargetList.targetList)
        {
            if (data.Value.Tags.HasFlag(TargetTypes.Missile))
            {
                Transform hud = null;
                if (_markers.ContainsKey(data.Value.transform))
                {
                    activeTargets.Add(data.Value.transform);
                    hud = _markers[data.Value.transform];
                }
                else
                {
                    hud = _hud.SpawnHudElement(TargetMarkerPrefab);
                }
                hud.GetComponent<WorldTo2DCameraConstraint>().target = data.Value.transform;
                newTargets.Add(data.Value.transform, hud);
            }
        }
        foreach (var marker in _markers)
        {
            if(!activeTargets.Contains(marker.Key)) _hud.DespawnHudElement(marker.Value);
        }
        _markers = newTargets;
    }
}