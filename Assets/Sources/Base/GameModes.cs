﻿using System;

namespace Base.Types
{
    [Flags]
    public enum GameModes : uint
    {
        None = 0x0,
        MainMenu = 0x1,
        Act = 0x2,
        Encounter = 0x4,
        Loading = 0x10
    }
}