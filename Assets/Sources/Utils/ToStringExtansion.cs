﻿using Entitas;

namespace Utils
{
    public static class ToStringExtansion
    {
        public static string ConvertToString(this IComponent component, string postfix = "")
        {
            if (component == null)
                return string.Empty;
            
#if (UNITY_EDITOR || UNITY_EDITOR_64)
            if(postfix.Length > 0)
                postfix = string.Concat(' ', postfix);
            return component.GetType().Name.Replace("Component", postfix);
#else
            return component.GetType().Name;
#endif
        }
    }
}