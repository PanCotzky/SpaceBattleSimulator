﻿using System.Collections;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;

public class HudManager : MonoBehaviour
{

	public Camera TargetCamera;
	public Camera HudCamera;
	
	public Transform SpawnHudElement(Transform prefab)
	{
		var item = PoolManager.Pools["Hud"].Spawn(prefab);
		item.transform.parent = transform;
		item.localScale = Vector3.one;
		var follow = item.GetComponent<WorldTo2DCameraConstraint>();
		follow.targetCamera = TargetCamera;
		follow.orthoCamera = HudCamera;

		return item;
	}

	public void DespawnHudElement(Transform element)
	{
		PoolManager.Pools["Hud"].Despawn(element);
	}
}
