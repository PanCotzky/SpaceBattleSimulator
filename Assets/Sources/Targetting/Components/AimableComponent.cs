﻿using System;
using System.Linq;
using Base.Types;
using Entitas;
using Utils;

namespace TacticalGame.Aim.Components
{
    /// <summary>
    /// Mark entity that it can be aimed.
    /// <remarks>
    /// Will be modified durring the feature run
    /// </remarks>
    /// </summary>
    [Targetting]
    public class AimableComponent : IComponent
    {
        public const int EmptyHitArea = -1;
        [Flags]
        public enum AimingType : uint
        {
            Unit = 0x1,
            Deck = 0x2,
            Miss = 0x4
        }

        public UniqueKey Key;
        public AimingType Type;
        public float Distance;

#if (UNITY_EDITOR || UNITY_EDITOR_64)
        private const string Broken = "Broken";

        //TODO
        public override string ToString()
        {
//            var context = Contexts.sharedInstance.gameState;
//            if (Type == AimingType.Miss)
//                return string.Format("Miss {0}", this.ConvertToString());
//
//            var entity = context.GetEntityWithPrimaryKey(Key);
//            return string.Format("{0} {1} {2}", this.ConvertToString(), 
//                entity != null ? entity.aAA.Comment : Broken, Type);
            return base.ToString();
        }
#endif
        
    }
    
    public static class AimableComponentExtension
    {
//        public static void AddAimable(this AimEntity entity, UniqueKey id,
//            AimableComponent.AimingType type = AimableComponent.AimingType.Deck)
//        {
//            entity.AddAimable(id, type, 0.0F);
//        }
    }
}