﻿using Base.Types;
using Entitas;
using UnityEngine;
using Utils;

namespace TacticalGame.Aim.Components
{
    [Targetting]
    public struct AimTargetComponent : IComponent
    {
        public UniqueKey Shooter;
        public UniqueKey Skill;

        public Vector2 Point;

        public override string ToString()
        {
            return this.ConvertToString();
        }
    }
}