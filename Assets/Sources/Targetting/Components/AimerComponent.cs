﻿using Base.Types;
using Entitas;
using Utils;

namespace TacticalGame.Aim.Components
{
    /// <summary>
    /// Mark an entity as aimer (shooter)
    /// </summary>
    [Targetting]
    public class AimerComponent : IComponent
    {
        /// <summary>
        /// Id of the skill entity that aiming
        /// </summary>
        public UniqueKey Skill { get; set; }
        public UniqueKey Room { get; set; }

        public override string ToString()
        {
            return this.ConvertToString();
        }
    }
}