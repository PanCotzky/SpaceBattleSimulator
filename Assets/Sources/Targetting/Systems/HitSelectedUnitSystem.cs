﻿using Base.Entitas;
using Base.Types;
using Entitas;
using TacticalGame.Aim.Components;
using UnityEngine;

namespace TacticalGame.Aim.Systems
{
//    internal sealed class HitSelectedUnitSystem : SingleReactiveSystem<TargettingEntity>, IInitializeSystem
//    {
//        private IGroup<TargettingEntity> _aimers;
//
//        private readonly AimContext _context;
//        private readonly GameStateContext _gameState;
//
//        public HitSelectedUnitSystem(AimContext context, GameStateContext gameState)
//            : base(context)
//        {
//            _context = context;
//            _gameState = gameState;
//        }
//
//        protected override ICollector<TargettingEntity> GetTrigger(IContext<TargettingEntity> context)
//        {
//            return context.CreateCollector(AimMatcher.AllOf(AimMatcher.PrimaryKey, AimMatcher.Selected,
//                AimMatcher.Raycaster));
//        }
//
//        public void Initialize()
//        {
//            _aimers = _context.GetGroup(AimMatcher.AllOf(AimMatcher.Aimer));
//        }
//
//        protected override bool Filter(TargettingEntity entity)
//        {
//            return entity.isEnabled && _aimers.count > 0 && !entity.hasAimTarget;
//        }
//
//        protected override void Execute(TargettingEntity entity)
//        {
//            if(!TargetIsValid(entity))
//                return;
//
//            AddAimTarget(entity);
//        }
//
//        private void AddAimTarget(TargettingEntity entity)
//        {
//            Vector3 position;
//            var hit = entity.raycaster.Raycast();
//            var key = UniqueKey.Empty;
//
//            if (hit.transform != null)
//            {
//                entity = null;
//                position = hit.point;
//                var hitted = (GameStateEntity) hit.collider.transform.GetEntity();
//                if (hitted != null)
//                    key = hitted.primaryKey;
//            }
//            else
//            {
//                position = _gameState.GetEntityWithPrimaryKey(entity.aimable.Key).position;
//                key = entity.aimable.Key;
//            }
//
//            AddAimTarget(entity, key, position);
//        }
//
//        private void AddAimTarget(TargettingEntity entity, UniqueKey hitted, Vector3 point)
//        {
//            var aimerEntity = _aimers.GetSingleEntity();
//            if (entity == null)
//            {
//                entity = _context.CreateEntity();
//                entity.AddAimable(hitted, AimableComponent.AimingType.Miss, aimerEntity.aimable.Distance);
//            }
//            entity.AddAimTarget(aimerEntity.aimable.Key, aimerEntity.aimer.Skill, point);
//        }
//
//        private bool TargetIsValid(TargettingEntity entity)
//        {
//            var aimerEntity = _aimers.GetSingleEntity();
//            var inRange = entity.InRangeOf(aimerEntity);
//            Debug.Log(inRange? "inRange" : "not in Range");
//            return inRange && aimerEntity.InducesTo(entity);
//        }
//    }
}