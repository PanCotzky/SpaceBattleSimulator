﻿namespace TacticalGame.Aim.Systems
{
    /// <summary>
    /// Initialize selectables units for specified aimers, previously releasing old selections.
    /// </summary>
//    internal sealed class UnitSelectablesInitSystem :  SingleReactiveSystem<AimEntity>, IInitializeSystem
//    {
//        private readonly IContext<AimEntity> _context;
//        //Will be needed for unit check in CanBeAim
//        // ReSharper disable once NotAccessedField.Local
//        private readonly IContext<GameStateEntity> _gameState;
//        private readonly List<AimEntity> _buffer;
//        
//        private IGroup<AimEntity> _availables;
//
//        public UnitSelectablesInitSystem(IContext<AimEntity> context, IContext<GameStateEntity> gameState)
//            : base(context)
//        {
//            _context = context;
//            _gameState = gameState;
//            _buffer = new List<AimEntity>();
//        }
//
//        public void Initialize()
//        {
//            _availables = _context.GetGroup(AimMatcher.AllOf(AimMatcher.PrimaryKey, AimMatcher.Aimable));
//        }
//
//        protected override ICollector<AimEntity> GetTrigger(IContext<AimEntity> context)
//        {
//            var aimer = AimMatcher.AllOf(AimMatcher.Aimable, AimMatcher.Aimer);
//            return context.CreateCollector(aimer.Added());
//        }
//
//        protected override bool Filter(AimEntity entity)
//        {
//            return true;
//        }
//
//        protected override void Execute(AimEntity aimer)
//        {
//            _availables.GetEntities(_buffer);
//            foreach (var entity in _buffer)
//            {
//                entity.isSelectable = entity.isSelected = false;
//                if(!CanBeAim(aimer, entity))
//                    continue;
//
//                entity.isSelectable = true;
//            }
//        }
//
//        private bool CanBeAim(AimEntity aimer, AimEntity aimbale)
//        {
//            if (aimer == aimbale)
//                return false;
//
//            return !aimbale.hasFleet || aimer.fleet.Id != aimbale.fleet.Id;
//        }
//    }
}