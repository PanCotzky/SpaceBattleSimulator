﻿namespace TacticalGame.Aim.Systems
{
//    internal sealed class UnitAimerInitiSystem : SingleReactiveSystem<GameStateEntity>, IInitializeSystem
//    {
//        private readonly IContext<AimEntity> _context;
//        private IGroup<AimEntity> _aimableGroup;
//
//        public UnitAimerInitiSystem(IContext<AimEntity> context, GameStateContext gameState)
//            : base(gameState)
//        {
//            _context = context;
//        }
//
//        public void Initialize()
//        {
//            _aimableGroup = _context.GetGroup(AimMatcher.AllOf(AimMatcher.PrimaryKey, AimMatcher.Aimable));
//        }
//
//        protected override ICollector<GameStateEntity> GetTrigger(IContext<GameStateEntity> context)
//        {
//            var selectedUnit = GameStateMatcher.AllOf(GameStateMatcher.Unit, GameStateMatcher.Selected);
//            return context.CreateCollector(selectedUnit.Added());
//        }
//
//        protected override bool Filter(GameStateEntity entity)
//        {
//            return entity.isEnabled;
//        }
//
//        protected override void Execute(GameStateEntity entity)
//        {
//            UniqueKey primaryKey = entity.primaryKey;
//            var aimbale = _aimableGroup.GetEntities().FirstOrDefault(e => e.aimable.Key == primaryKey);
//            if (aimbale == null)
//            {
//                Debug.LogWarning("Has no posible aimers for primary key " + primaryKey);
//                return;
//            }
//            
//            aimbale.ReplaceAimer(UniqueKey.Empty, UniqueKey.Empty);
//        }
//    }
}