﻿namespace TacticalGame.Aim.Systems
{
    /// <summary>
    /// This system mark Units as aimable entities.
    /// </summary>
//    internal sealed class UnitAimableInitSystem : SingleReactiveSystem<GameStateEntity>
//    {
//        private readonly IContext<AimEntity> _context;
//
//        public UnitAimableInitSystem(IContext<AimEntity> context, IContext<GameStateEntity> gameState)
//            : base(gameState)
//        {
//            _context = context;
//        }
//
//        protected override ICollector<GameStateEntity> GetTrigger(IContext<GameStateEntity> context)
//        {
//            //TODO: Add other units 
//            var usnitWithMainDeck = GameStateMatcher.AllOf(GameStateMatcher.Unit).Added();
//            return context.CreateCollector(usnitWithMainDeck);
//        }
//
//        protected override bool Filter(GameStateEntity entity)
//        {
//            return true;
//        }
//
//        protected override void Execute(GameStateEntity entity)
//        {
//            var aimEntity = _context.CreateEntity();
//            aimEntity.AttachPrimaryKey();
//            aimEntity.AddAimable(entity.primaryKey, AimableComponent.AimingType.Unit);
//            aimEntity.AddFleet(entity.GetDirect(GameStateMatcher.Fleet).fleet.Id);
//        }
//    }
}