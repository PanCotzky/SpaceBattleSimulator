﻿namespace TacticalGame.Aim.Systems
{
//    internal sealed class UnitReleaseSelectionSystem : SingleReactiveSystem<InputEntity>, IInitializeSystem
//    {
//        private readonly AimContext _context;
//        private IGroup<AimEntity> _aimables;
//
//        public UnitReleaseSelectionSystem(AimContext context, InputContext input) : base(input)
//        {
//            _context = context;
//        }
//
//        protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
//        {
//            return context.CreateCollector(InputMatcher.Transform.Removed());
//        }
//
//        public void Initialize()
//        {
//            var aimables = AimMatcher.AllOf(AimMatcher.Aimable, AimMatcher.AimTarget);
//            _aimables = _context.GetGroup(aimables);
//        }
//
//        protected override bool Filter(InputEntity inputEntity)
//        {
//            return _aimables.count > 0;
//        }
//
//        protected override void Execute(InputEntity inputEntity)
//        {
//            var aimables = _aimables.GetEntities();
//            foreach (var entity in aimables)
//            {
//                entity.isSelected = false;
//                if (entity.hasRaycaster)
//                    entity.RemoveRaycaster();
//                if (entity.hasAimTarget)
//                    entity.RemoveAimTarget();
//
//                if(entity.aimable.Type == AimableComponent.AimingType.Miss)
//                    entity.Destroy();
//            }
//        }
//    }
}