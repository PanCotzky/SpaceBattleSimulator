﻿using System.Linq;
using Entitas;
using Entitas.Unity;


namespace TacticalGame.Aim.Systems
{
//    internal sealed class UnitSelectionSystem : SingleReactiveSystem<InputEntity>, IInitializeSystem
//    {
//        private IGroup<AimEntity> _aimer;
//        private IGroup<AimEntity> _selectables;
//
//        private readonly SkillsContext _skills;
//        private readonly IContext<AimEntity> _context;
//        private IGroup<SkillsEntity> _selectedSkills;
//
//        public UnitSelectionSystem(IContext<AimEntity> context, InputContext input, SkillsContext skills)
//            : base(input)
//        {
//            _context = context;
//            _skills = skills;
//        }
//
//        protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
//        {
//            return context.CreateCollector(InputMatcher.Transform.Added());
//        }
//
//        public void Initialize()
//        {
//            var selectables = AimMatcher.AllOf(AimMatcher.PrimaryKey, AimMatcher.Aimable, AimMatcher.Selectable);
//            var selectedSkillWithRange = SkillsMatcher.AllOf(SkillsMatcher.Skill, SkillsMatcher.Selected)
//                .Effects(SkillsMatcher.Range);
//
//            _selectables = _context.GetGroup(selectables);
//            _aimer = _context.GetGroup(AimMatcher.AllOf(AimMatcher.Aimer, AimMatcher.Fleet));
//            _selectedSkills = _skills.GetGroup(selectedSkillWithRange);
//        }
//
//        protected override bool Filter(InputEntity inputEntity)
//        {
//            return _aimer.count > 0 && _selectedSkills.count > 0 && _selectables.count > 0;
//        }
//
//        protected override void Execute(InputEntity input)
//        {
//            var entityLink = input.transform.GetEntityLink();
//            if (entityLink == null)
//                return;
//
//            var entity = entityLink.entity as GameStateEntity;
//            if (entity == null)
//                return;
//
//            var aimableKey = entity.primaryKey;
//            if (!entity.hasUnit)
//            {
//                var unit = entity.GetDirect(GameStateMatcher.Unit);
//                if (unit != null)
//                    aimableKey = unit.primaryKey;
//            }
//
//            var aimable = _selectables.GetEntities().FirstOrDefault(e => e.aimable.Key == aimableKey);
//            if (aimable == null)
//                return;
//
//            aimable.AddRaycaster(_aimer.GetSingleEntity().aimable.Key, aimable.aimable.Key, GameLayers.HitColliders);
//            aimable.isSelected = true;
//        }
//    }
}