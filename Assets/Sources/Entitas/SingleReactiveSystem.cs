﻿using System.Collections.Generic;
using Entitas;

namespace Base.Entitas
{
    public abstract class SingleReactiveSystem<TEntity> : ReactiveSystem<TEntity>
        where TEntity : class, IEntity
    {
        protected SingleReactiveSystem(IContext<TEntity> context) : base(context)
        {
        }

        protected SingleReactiveSystem(ICollector<TEntity> collector) : base(collector)
        {
        }

        protected sealed override void Execute(List<TEntity> entities)
        {
            foreach (var entity in entities)
                Execute(entity);
        }

        protected abstract void Execute(TEntity entity);
    }
}