﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;

namespace Base.Entitas
{
    public static class EntitasContextsExtensions
    {
        private static readonly Dictionary<string, int> ContextNamedIndices = new Dictionary<string, int>();
        private static readonly Dictionary<Type, int> ContextTypedIndices = new Dictionary<Type, int>();

        // ReSharper disable once UnusedParameter.Global
        public static int GetContextIndex<TEntity>(this Contexts contexts)
        {
            var entityType = typeof(TEntity);
            if(!ContextTypedIndices.ContainsKey(entityType))
                InitializeContextIndices();

            return ContextTypedIndices[entityType];
        }

        public static int GetContextIndex(this IEntity entity)
        {
            if (!entity.isEnabled)
                throw new ArgumentException("Entity disabled!");

            var contextName = entity.contextInfo.name;
            if (!ContextNamedIndices.ContainsKey(contextName))
                InitializeContextIndices();

            return ContextNamedIndices[contextName];
        }

        private static void InitializeContextIndices()
        {
            //If something wrong will fall
            var allContexts = Contexts.sharedInstance.allContexts;
            for (var index = 0; index < allContexts.Length; index++)
            {
                var contextName = allContexts[index].contextInfo.name;
                var type = allContexts[index].GetType().BaseType;
                // ReSharper disable once PossibleNullReferenceException
                var entityType = type.GetGenericArguments().First();
                ContextNamedIndices.Add(contextName, index);
                ContextTypedIndices.Add(entityType, index);
            }
        }
    }
}