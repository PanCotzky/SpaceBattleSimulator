using UnityEngine;
using System.Collections;

public class MissileLauncher: MonoBehaviour {

	public Transform missilePrefab;	   // what to fire
	public Transform spawnPoint;       // where to fire it from
	public Vector3 launchVelocity;     // how fast, and in what direction, to eject the missile
	public Camera missileCam;          // optional missile cam, just for fun really
	public string fireButton = "Fire1";
	public Transform Target;
	
	private Rigidbody body;

	public void Start() {
		body = transform.root.GetComponentInChildren<Rigidbody>();
		if (Target == null)
		{
			GameObject target = GameObject.FindWithTag("Target");
			Target = target.transform;
		}
	}

	public void Update() {
		// demo target selection: just find a game object tagged "Target"
		if (Input.GetButtonDown(fireButton)) {
//			GameObject target = GameObject.FindWithTag("Target");
			if (Target) {
				Fire(Target);
			} else {
				Debug.Log("No target found! Tag something as \"Target\" in your scene.");
			}
		}
	}

	public void Fire(Transform target) {
		Transform missile = (Transform) Instantiate(missilePrefab, spawnPoint.position, transform.rotation);
		missile.GetComponent<Rigidbody>().velocity = body.velocity + transform.rotation * launchVelocity;
		missile.GetComponent<GuidedMissile>().Fire(this, target.transform);

		if (missileCam) {
			// have the camera follow the missile
			// the missile will deactivate it when it hits
			missileCam.transform.parent = missile;
			missileCam.transform.rotation = transform.rotation;
//			missileCam.transform.localRotation = Quaternion.identity;
//			missileCam.transform.localPosition = new Vector3(0, 0, 1);
			missileCam.enabled = true;
		}
	}
}