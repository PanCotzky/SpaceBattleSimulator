using UnityEngine;

// Augmented Proportional Navigation missile guidance.
// 
// For more information on the algorithm:
// - http://en.wikipedia.org/wiki/Proportional_navigation
// - http://trajectorysolution.com/HomingGuidance.html

public class GuidedMissile: MonoBehaviour {

//	public Transform explosionPrefab;
	public Transform target;

	public float maxTurnRate = 180.0f;	// turn rate in degrees per second
	public float maxThrust = 10.0f;		// maximum acceleration
	public float thrustRamp = 3.0f;		// how quickly full thrust becomes available
	public float turnRamp = 3.0f;		// how quickly full turning becomes available
	public float nc = 3.0f;				// gain, usually between 3 and 5
	public float ignitionDelay = 1.0f;	// delay between launch and activating thruster
	public bool destroyTrail = true;  // destroy trail on impact
	public MF_FxController effects;
	
	float startTime;
	Rigidbody body;
	ParticleSystem thruster;
	TrailRenderer trail;
	float thrust = 0.0f;
	float turnRate = 0.0f;
	Vector3 los;  // line of sight
	Vector3 acceleration;

	public void Start() {
		effects = GetComponentInChildren<MF_FxController>();
		body = transform.root.GetComponentInChildren<Rigidbody>();
		thruster = GetComponentInChildren<ParticleSystem>();
		thruster.Stop();
		trail = GetComponentInChildren<TrailRenderer>();
		trail.enabled = false;
	}

	public void Fire(MissileLauncher launcher, Transform target) {
		startTime = Time.time;
		this.target = target;

		// don't collide with our own launcher
		Collider missileCollider = GetComponentInChildren<Collider>();
		Collider[] launcherColliders = launcher.transform.root.GetComponentsInChildren<Collider>();
		var proj = GetComponent<MF_B_Projectile>();
		proj.duration = 1000;
		foreach (Collider collider in launcherColliders) {
			Physics.IgnoreCollision(missileCollider, collider, true);
		}
	}

	public void FixedUpdate() {
		if (Time.time - startTime < ignitionDelay) {
			return;
		}

		thruster.Play();
		trail.enabled = true;

		if (target) {
			// build up to maximum thrust and turn rate
			if (thrust < maxThrust) {
				// don't go over in case thrustRamp is very small
				float increase = Time.fixedDeltaTime * maxThrust/thrustRamp;
				thrust = Mathf.Min(thrust + increase, maxThrust);
			}

			if (turnRate < maxTurnRate) {
				float increase = Time.fixedDeltaTime * maxTurnRate/turnRamp;
				turnRate = Mathf.Min(turnRate + increase, maxTurnRate);
			}

			// Proportional Navigation evaluates the rate of change of the 
			// Line Of Sight (los) to our target. If the rate of change is zero,
			// the missile is on a collision course. If it is not, we apply an
			// acceleration to correct course.
			Vector3 prevLos = los;
			los = target.position - transform.position;
			Vector3 dLos = los - prevLos;

			// we only want the component perpendicular to the line of sight
			dLos = dLos - Vector3.Project(dLos, los);
			
			// plain PN would be:
			// acceleration = Time.fixedDeltaTime*los + dLos * nc;

			// augmented PN in addition takes acceleration into account
			acceleration = Time.fixedDeltaTime*los + dLos * nc + Time.fixedDeltaTime*acceleration*nc/2;
			
			// limit acceleration to our maximum thrust
			acceleration = Vector3.ClampMagnitude(acceleration * thrust, thrust);
			
			// draw some debug lines to visualize the output of the algorithm {
			// velocity, LOS rotation rate and resulting acceleration
			// Debug.DrawLine(transform.position, transform.position + body.velocity, Color.green);
			// Debug.DrawLine(transform.position, transform.position + dLos/Time.fixedDeltaTime, Color.red);
			// Debug.DrawLine(transform.position, transform.position + acceleration, Color.blue);
			
			// to immediately accelerate towards the target {
			// body.AddForce(acceleration, ForceMode.Acceleration);

			// instead, turn our body towards it and apply forward thrust
			// this makes the missiles less perfect and more realistic
			Quaternion targetRotation = Quaternion.LookRotation(acceleration, transform.up);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.deltaTime * turnRate);
			body.AddForce(transform.forward * acceleration.magnitude, ForceMode.Acceleration);
		} else {
			// target lost, self destruct
			Detonate(null);
		}
	}

	void OnTriggerEnter(Collider other) {
		// should missiles collide with other missiles?
		// for this demo, they do not
		if (other.transform.root.GetComponent<GuidedMissile>()) {
			return;
		}
		
		// hit!
		Detonate(other);
	}

	public void Detonate(Collider hit) {
		if (hit) {
			// we hit something - do something to it, for example:
			// Destroy(hit.gameObject)
			// but for the demo, we just do nothing
		}
		
		// deactivate the missile cam
		Camera cam = GetComponentInChildren<Camera>();
		if (cam) {
			cam.transform.parent = null;
			cam.enabled = false;
		}

		if (!destroyTrail) {
			// detach trail so it will remain behind
			trail.transform.parent = null;
		}

//		Transform explosion = (Transform) Instantiate(explosionPrefab, transform.position, Quaternion.identity);
//		explosion.GetComponent<Rigidbody>().velocity = body.velocity;
		Destroy(this.gameObject);
	}
}
