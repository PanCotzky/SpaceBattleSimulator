using UnityEngine;
using System.Collections;

public class Explosion: MonoBehaviour {

	public float flashDuration = 0.25f;

	public void Start() {
		StartCoroutine(Flash());
	}

	public IEnumerator Flash() {
		Light flash = GetComponent<Light>();
		float intensity = flash.intensity;
		float start = Time.time;
		while (flash.intensity > 0) {
			flash.intensity = Mathf.Lerp(intensity, 0, (Time.time - start)/flashDuration);
			yield return 0;
		}
	}

	public void Update() {
		// wait for all the particle effects to end, then autodestruct
		foreach (ParticleSystem particleSystem in GetComponentsInChildren<ParticleSystem>()) {
			if (particleSystem.IsAlive()) {
				return;
			}
		}
		
		Destroy(gameObject);
	}
}
