using UnityEngine;

public class FighterMover: MonoBehaviour {

	public float speed = 2.0f;
	public float distance = 200.0f;
	private Vector3 _baseOrientation;
	
	Vector3 drive;

	public void Start() {
		drive = speed * transform.right;
		_baseOrientation = transform.rotation.ToEuler();
	}

	public void FixedUpdate() {
		GetComponent<Rigidbody>().AddForce(drive, ForceMode.VelocityChange);

		if (transform.position.x > distance) {
			drive = -speed * Vector3.right;
		}

		if (transform.position.x < -distance) {
			drive = speed * Vector3.right;
		}

		int dir = GetComponent<Rigidbody>().velocity.x > 0 ? -1 : 1;
//		transform.rotation = Quaternion.Euler(_baseOrientation.x, _baseOrientation.y, _baseOrientation.z + dir * GetComponent<Rigidbody>().velocity.magnitude);
	}
}