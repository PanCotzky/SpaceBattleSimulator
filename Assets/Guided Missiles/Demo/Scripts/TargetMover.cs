using UnityEngine;

public class TargetMover: MonoBehaviour {

	public float speed = 2.0f;
	public float distance = 200.0f;

	Vector3 v;

	public void Start() {
		v = speed * transform.right;
	}

	public void FixedUpdate() {
		GetComponent<Rigidbody>().AddForce(v, ForceMode.VelocityChange);
		if (transform.position.x > distance) {
			v = -speed * transform.right;
		}
		if (transform.position.x < -distance) {
			v = speed * transform.right;
		}
	}
}