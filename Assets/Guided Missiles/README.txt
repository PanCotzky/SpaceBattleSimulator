Pro-Nav Guided Missiles
-----------------------
To get started, please examine one of the demonstration scenes. The "Fighter" demo shows missiles launched from a fighter aircraft: they drop from underneath the wing by gravity. The "Ground" demo launches them from a silo, with the launcher giving the missile some initial forward velocity.

Tweaking missile behaviour
--------------------------
The missile script exposes the variables you can use to tweak its behaviour in the inspector.

NC, the "navigational constant" determines how much prediction the proportional navigation algorithm applies: how much it will overcorrect for being off course. The recommended value in the literature is between 3 and 5.

Drag on the missile's Rigidbody is an important parameter: too little drag and it will tend to overshoot its target, and have trouble cornering. High drag, combined with enough thrust, results in a missile with lots of "grip". Play with the values until you get something that works for your game!

Support
-------
This package was created by Michiel Konstapel. If you have any issues, questions, etc, please contact me at support@weefreestudio.com.
